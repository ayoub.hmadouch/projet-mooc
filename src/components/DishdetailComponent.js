import React from "react";
import { Card, CardBody, CardImg, CardText, CardTitle } from "reactstrap";

function RenderDish({ dish }) {
	if (dish != null) {
		return (
			<div className="col-md-5 mt-4 mb-4">
				<Card>
					<CardImg top src={dish.image} alt={dish.name} />
					<CardBody>
						<CardTitle>{dish.name}</CardTitle>
						<CardText>{dish.description}</CardText>
					</CardBody>
				</Card>
			</div>
		);
	} else {
		return <div></div>;
	}
}
function RenderComments({ comments, addComment, dishId }) {
	if (comments != null) {
		return (
			<div className="col-md-5 mt-4 mb-4">
				<Card>
					<h4 className="m-3">Comments</h4>
					<CardBody>
						{comments.map((comment) => {
							return (
								<CardText key={comment.id}>
									<p>{comment.comment}</p>
									<p>
										--{comment.author},
										{new Intl.DateTimeFormat("en-US", {
											year: "numeric",
											month: "short",
											day: "2-digit",
										}).format(
											new Date(Date.parse(comment.date))
										)}
									</p>
								</CardText>
							);
						})}
					</CardBody>
				</Card>
			</div>
		);
	} else {
		return <div></div>;
	}
}
const Dishdetail = (props) => {
	return (
		<div className="row justify-content-center">
			<RenderDish dish={props.dish} />
			<RenderComments comments={props.comments} />
		</div>
	);
};

export default Dishdetail;
