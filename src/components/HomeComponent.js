import React from "react";
import {
	Breadcrumb,
	BreadcrumbItem,
	Card,
	CardBody,
	CardImg,
	CardSubtitle,
	CardText,
	CardTitle,
} from "reactstrap";
function RenderCard({ item }) {
	return (
		<Card>
			<CardImg src={item.image} alt={item.name} />
			<CardBody>
				<CardTitle>{item.name}</CardTitle>
				{item.designation ? (
					<CardSubtitle>{item.designation}</CardSubtitle>
				) : null}
				<CardText>{item.description}</CardText>
			</CardBody>
		</Card>
	);
}
const Home = (props) => {
	return (
		<div className="container mt-2">
			<div className="row align-items-start">
				<div class="row">
					<Breadcrumb>
						<BreadcrumbItem active>Home</BreadcrumbItem>
					</Breadcrumb>
					<div className="col-12">
						<h3>Home</h3>
						<hr />
					</div>
				</div>

				<div className="col-12 col-md m-1">
					<RenderCard item={props.dish} />
				</div>
				<div className="col-12 col-md m-1">
					<RenderCard item={props.promotion} />
				</div>
				<div className="col-12 col-md m-1">
					<RenderCard item={props.leader} />
				</div>
			</div>
		</div>
	);
};

export default Home;
