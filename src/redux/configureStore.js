import { createStore, combineReducers } from "redux";
import { Dishes } from "./dishReducer";
import { Promotions } from "./promotionReducer";
import { Leaders } from "./leadersReducer";
import { Comments } from "./commentsReducer";

export const ConfigureStore = () => {
	const allReducers = combineReducers({
		dishes: Dishes,
		promotions: Promotions,
		leaders: Leaders,
		comments: Comments,
	});
	const store = createStore(
		allReducers,
		window.__REDUX_DEVTOOLS_EXTENSION__ &&
			window.__REDUX_DEVTOOLS_EXTENSION__()
	);
	return store;
};
